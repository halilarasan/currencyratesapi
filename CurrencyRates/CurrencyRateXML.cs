﻿using CurrencyRates.Model;
using Repositories.Interface;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyRates
{
    public class CurrencyRateXML : CurrencyRateBase
    {
        public CurrencyRateXML(ICurrencyRepository currencyRepository) : base(currencyRepository)
        {

        }

        public override async Task<string> GetCurrencyRateAsync(CurrencyParams currencyParams)
        {
            var tarih_date = await GetTodayCurrencies();
            tarih_date = GetFilteredCurrrencies(tarih_date, currencyParams);
            return tarih_date.Currency.ToXml();
        }
    }
}
