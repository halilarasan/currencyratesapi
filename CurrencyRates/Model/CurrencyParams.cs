﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CurrencyRates.Model
{
    public class CurrencyParams
    {
        public string CurrencyCode { get; set; } = "";
        public List<Sort> SortParams { get; set; } = new List<Sort>();
    }

    public class Sort
    {
        public SortField FieldName { get; set; }
        public SortDirection Direction { get; set; }
    }

    public enum SortField
    {
        ForexBuying,
        ForexSelling,
        BanknoteBuying,
        BanknoteSelling
    }
    public enum SortDirection
    {
        Asc,
        Desc
    }
}
