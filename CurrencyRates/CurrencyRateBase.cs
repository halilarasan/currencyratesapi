﻿using CurrencyRates.Interface;
using CurrencyRates.Model;
using Repositories.Interface;
using Repositories.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonUtilities.Extensions;

namespace CurrencyRates
{
    public abstract class CurrencyRateBase : ICurrencyRate
    {
        private ICurrencyRepository _currencyRepository;

        public CurrencyRateBase(ICurrencyRepository currencyRepository)
        {
            _currencyRepository = currencyRepository;

        }
        protected async Task<Tarih_Date> GetTodayCurrencies()
        {
            return await _currencyRepository.GetTodayCurrencies();
        }

        protected Tarih_Date GetFilteredCurrrencies(Tarih_Date tarih_date,CurrencyParams currencyParams)
        {
            if (!string.IsNullOrEmpty(currencyParams.CurrencyCode))
                tarih_date.Currency = tarih_date.Currency.Where(w => w.CurrencyCode == currencyParams.CurrencyCode).ToList();

            if (currencyParams.SortParams.Count > 0)
            {
                IQueryable<Currency> list = tarih_date.Currency.AsQueryable();
                foreach (var sort in currencyParams.SortParams)
                {
                    tarih_date.Currency = list.OrderBy(sort.FieldName.ToString(), sort.Direction == SortDirection.Desc).ToList();
                }
            }
            return tarih_date;
        }

        public abstract Task<string> GetCurrencyRateAsync(CurrencyParams currencyParams);
    }
}
