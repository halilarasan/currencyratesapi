﻿using CurrencyRates.Interface;
using CurrencyRates.Model;
using Repositories.Interface;
using Repositories.Model;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyRates
{
    public class CurrencyRateCSV : CurrencyRateBase
    {

        public CurrencyRateCSV(ICurrencyRepository currencyRepository) : base(currencyRepository)
        {

        }

        public override async Task<string> GetCurrencyRateAsync(CurrencyParams currencyParams)
        {
            var tarih_date = await GetTodayCurrencies();
            tarih_date = GetFilteredCurrrencies(tarih_date, currencyParams);
            return tarih_date.Currency.ToCsv();
        }
    }
}
