﻿using CurrencyRates.Model;
using Repositories.Model;
using Repositories.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyRates
{
    public class CurrencyRateAPI
    {
        private string _userName;
        private string _password;

        public CurrencyRateAPI(string userName,string password)
        {
            _userName = userName;
            _password = password;
        }

        public async Task<string> GetCurrencyRateCSVAsync(CurrencyParams currencyParams)
        {
            var userRepository = new UserRepository();

            if (!userRepository.Authenticate(_userName, _password))
                return "Authenticate failed!";

            var currencyRepository = new CurrencyRepository();
            var currencyCsv = new CurrencyRateCSV(currencyRepository);
            return await currencyCsv.GetCurrencyRateAsync(currencyParams);
        }

        public async Task<string> GetCurrencyRateXMLAsync(CurrencyParams currencyParams)
        {
            var userRepository = new UserRepository();

            if (!userRepository.Authenticate(_userName, _password))
                return "Authenticate failed!";

            var currencyRepository = new CurrencyRepository();
            var currencyCsv = new CurrencyRateXML(currencyRepository);
            return await currencyCsv.GetCurrencyRateAsync(currencyParams);
        }
    }
}
