﻿using CurrencyRates.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyRates.Interface
{
    interface ICurrencyRate
    {
        Task<string> GetCurrencyRateAsync(CurrencyParams currencyParams);
    }
}
