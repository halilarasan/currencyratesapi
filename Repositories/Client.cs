﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using CommonUtilities.Extensions;

namespace Repositories
{
    public class Client : IDisposable
    {
        private static Client _instance = null;
        public static Client Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Client();
                }
                return _instance;
            }
        }

        private static readonly string strAddress = "https://www.tcmb.gov.tr/kurlar/";

        private readonly Uri _baseAddress = new Uri(strAddress);

        private readonly HttpClient _client = new HttpClient
        {
            BaseAddress = new Uri(strAddress),
            Timeout = TimeSpan.FromSeconds(360)
        };

        public Client()
        {
            if (!_baseAddress.IsWellFormedOriginalString() && !string.IsNullOrWhiteSpace(_baseAddress.ToString()))
            {
                throw new Exception("Base address must set while creating a Client instance");
            }

            _client.BaseAddress = _baseAddress;
            _client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public Task<T> GetAsync<T>(string endpoint, object request = null) where T : class
        {
            try
            {
                var req = request != null ? "?" + request.GetQueryString() : "";
                return Invoke(
                    client => client.GetAsync($"{endpoint}{req}"),
                    async message => await DeserializeObjectAsync<T>(await message.Content.ReadAsStringAsync())
                );
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Task<T> DeleteAsync<T>(string endpoint, object request = null) where T : class
        {
            var req = request != null ? "?" + request.GetQueryString() : "";
            return Invoke(
                client => client.DeleteAsync($"{endpoint}{req}"),
                async message => await DeserializeObjectAsync<T>(await message.Content.ReadAsStringAsync())
            );
        }

        private async Task<T> Invoke<T>(
            Func<HttpClient, Task<HttpResponseMessage>> operation,
            Func<HttpResponseMessage, Task<T>> actionOnResponse)
        {
            var response = await operation(_client).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
                return await actionOnResponse(response).ConfigureAwait(false);

            var exception = new Exception(await response.Content.ReadAsStringAsync());
            exception.Data.Add("StatusCode", (int)response.StatusCode);
            throw exception;
        }

        private async Task<T> DeserializeObjectAsync<T>(string xml) where T : class
        {
            var result = xml.ParseXML<T>();
            return await Task.FromResult(result);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
