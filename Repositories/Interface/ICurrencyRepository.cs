﻿using Repositories.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Interface
{
    public interface ICurrencyRepository
    {
        Task<Tarih_Date> GetTodayCurrencies();
    }
}
