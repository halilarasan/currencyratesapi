﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Interface
{
    interface IUserRepository
    {
        bool Authenticate(string userName, string password);
    }
}
