﻿using Repositories.Interface;
using Repositories.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repository
{
    public class CurrencyRepository : ICurrencyRepository
    {
        public Task<Tarih_Date> GetTodayCurrencies()
        {
            return Client.Instance.GetAsync<Tarih_Date>("today.xml");
        }
    }
}
