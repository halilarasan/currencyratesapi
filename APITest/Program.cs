﻿using CurrencyRates;
using CurrencyRates.Model;
using System;
using System.Collections.Generic;

namespace APITest
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var currencyApi = new CurrencyRateAPI("halil", "345");
            var currencyParams = new CurrencyParams();
            var currencyList = await currencyApi.GetCurrencyRateXMLAsync(currencyParams);
            Console.ReadLine();
        }
    }
}
