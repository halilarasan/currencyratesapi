﻿using CurrencyRates;
using CurrencyRates.Model;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest
{
    public class CurrencyRateAPITest
    {
        [Fact]
        public async Task CSV_API_Result_Should_Difference_From_Null()
        {
            var currencyApi = new CurrencyRateAPI("halil", "345");
            var currencyParams = new CurrencyParams();
            var result = await currencyApi.GetCurrencyRateCSVAsync(currencyParams);
            (string.IsNullOrEmpty(result)).ShouldBe(false);
        }

        [Fact]
        public async Task XML_API_Result_Should_Difference_From_Null()
        {
            var currencyApi = new CurrencyRateAPI("halil", "345");
            var currencyParams = new CurrencyParams();
            var result = await currencyApi.GetCurrencyRateXMLAsync(currencyParams);
            (string.IsNullOrEmpty(result)).ShouldBe(false);
        }
    }
}
