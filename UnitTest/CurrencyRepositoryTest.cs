﻿using Repositories.Repository;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest
{
    public class CurrencyRepositoryTest
    {
        [Fact]
        public async Task Get_Currency_List_Should_More_Than_One()
        {
            var currencyRepository = new CurrencyRepository();
            var currency = await currencyRepository.GetTodayCurrencies();
            bool isMoreThanOne = currency.Currency.Count > 1;
            isMoreThanOne.ShouldBe(true);
        }
    }
}
