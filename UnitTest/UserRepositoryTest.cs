using Repositories.Repository;
using Shouldly;
using System;
using Xunit;

namespace UnitTest
{
    public class UserRepositoryTest
    {
        [Fact]
        public void Send_UserName_And_Password_Should_Return_True()
        {
            var userRepository = new UserRepository();
            var result = userRepository.Authenticate("halil", "345");
            result.ShouldBe(true);
        }

        [Fact]
        public void Send_UserName_And_Password_Should_Return_False()
        {
            var userRepository = new UserRepository();
            var result = userRepository.Authenticate("wrongUser", "123qwe");
            result.ShouldBe(false);
        }
    }
}
