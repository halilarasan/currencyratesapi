**Running Unit Test**

 - Firstly, you should open solution with Visual Studio via CurrentyRatesAPI.sln file.
 - Then, right click the UnitTest project and click "Run Tests"

**Used 3rd party libraries**

 - Shouldly
 - ServiceStack.Text

**Used Design Patterns**

 - Dependency Injection
 - Repository
 - Singleton

**Trade-offs / Possible performance issues**

 - Authentication should have session mechanism. Now, we have to check permission each request.
 - Currency rate data is change daily. We can use cache mechanism. So we can decrease response time.


> Note: I added the Console App to solution for debugging.
